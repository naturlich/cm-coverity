#!/bin/sh

function deploy_docker()
{
	sh ./common/deploy.sh
}

function create_container()
{
	local ARCH=$1
	local PROJECT=$2
	local TIME=$3

	sh docker_deploy/container_setup.sh ${ARCH} "${PROJECT}-${TIME}"
	[ $? = 0 ] || exit 1
}

function destroy_container()
{
	local CONTAINER=$1

	/usr/bin/docker rm -f ${CONTAINER}
}

if [ "$#" -ne 4 ]; then
	echo "Usage: $0 project-name branch-name local-source-path git-source-path" >&2
	exit 1
fi

PROJECT=$1
BRANCH=$2
LOCAL_SRCPATH=$3
GIT_SRCPATH=$4

ARCH=x86_64

TIME="`date +%s`"
CONTAINER="${USER}-${ARCH}-${PROJECT}-${TIME}"
DOCKER_EXEC="docker exec -i ${CONTAINER} bash -c"

# Setup
deploy_docker
create_container ${ARCH} ${PROJECT} ${TIME}

# Run
${DOCKER_EXEC} "[ -d ${LOCAL_SRCPATH} ] || git clone ${GIT_SRCPATH} ${LOCAL_SRCPATH}"

CURRENT_BRANCH=`git branch | grep \* | cut -d ' ' -f2`
if [ x"$CURRENT_BRANCH" != x"$BRANCH" ]; then
	${DOCKER_EXEC} "cd ${LOCAL_SRCPATH} && git checkout -b ${BRANCH} origin/${BRANCH}"
fi
${DOCKER_EXEC} "cd ${LOCAL_SRCPATH} && git clean -fdx"
${DOCKER_EXEC} "cd ${LOCAL_SRCPATH} && git reset --hard HEAD"
${DOCKER_EXEC} "cd ${LOCAL_SRCPATH} && git pull --rebase origin ${BRANCH}"

${DOCKER_EXEC} "cd ${LOCAL_SRCPATH} && scripts/coverity.sh"

${DOCKER_EXEC} "rm -rf ${LOCAL_SRCPATH}"

# clean up
destroy_container ${CONTAINER}
